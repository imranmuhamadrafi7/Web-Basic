// Sebelum refactoring
function hitungLuasSegitiga(alas, tinggi) {
    var luas = 0;
    luas = (alas * tinggi) / 2;
    return luas;
  }
  
  // Setelah refactoring
  function hitungLuasSegitiga(alas, tinggi) {
    return (alas * tinggi) / 2;
  }
  