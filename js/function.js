/*
!FUNTION 
adalah sebuah sub-program/sub routine yang dapat 'dipanggil' di bagian lain pada program
- untuk menggunakannya, kita harus 'membuat' nya terlebih dahulu dan lalu 'Memanggilnya'

* KATAGORI FUNCTION
- Built-in Function
    adalah function yg sudah disediakan / dibuat oleh javascript
- kita tinggak memanggilnya saja :
    Contoh :
    - Alert();
    - prompt();
    - confirm();
    Contoh Lain :
    - charAt(), slice(), split(), toString(), toLowerCase(), toUpperCase(), ...
    Contoh Math :
    - sin(), cos(), tan(), random(), round(), floor(), ceil(), log() ....
- User-Defined Function
    adalah function yang kita buat sendiri
    - untuk membuat funtion kita bisa menggunakan keyoword function(namaFunction)
    - funtion memiliki parameter/argument disimpan kedalam (sini). boleh ada dan boleh juga tidak. jika ada boleh lebih dari satu
        dipisahkan oleh koma (,)
    - function body, dibungkus dengan {kurung_kurawal}
    - funtion juga dapat mengembalikan nilai (return value)
    - ada dua cara untuk membuat user-defined function
        *dengan deklarasi/function declaration
        *dengan expresi/function expression

*/

// CONTOH FUNCTION USER-DEFINED 
function sayHello(nama){
    console.log('Hello '+nama+'!');
}
nama('Imran Muhamad Rafi');
/*
! PENJELASAN PROGRAM :
Di dalam contoh di atas, fungsi sayHello() memiliki satu parameter yaitu nama. 
Saat fungsi tersebut dipanggil dengan nilai "Imran Muhamad Rafi", 
maka parameter name akan diisi dengan nilai "Imran Muhamad Rafi". 
Kemudian, nilai "Imran Muhamad Rafi" akan digunakan di dalam fungsi untuk mencetak pesan "Hello, Imran Muhamad Rafi!".
*/

//CONTOH DECLARATION FUNCTION
function jmlDuaBil (a, b){
    var total ;
    total = a + b;
    //fungsi ini akan mengembalikan nilai dari a + b;
    return total;
}
// ini untuk memanggil sebuah funtion dengan mengisi nilainya, maka akan menampilkan outpunya yaitu 3
alert(jmlDuaBil(1,2));

//CONTOH FUNCTION LAIN
function jmlhBil(bil1, bil2){
    return bil1 + bil2;
}
var bil1 = parseInt(prompt('Masukan Bilangan 1 :'));
var bil2 = parseInt(prompt('Masukan Bilangan 2 :'));
var hasil = jmlhBil(bil1*2,bil2*2);
console.log(hasil);

// CONTOH LAIN
function tambah (a,b){
    return a+b;
}
function kali (a, b){
    return a*b;
}
var hasil = kali(tambah(5, 5), tambah(3,2));
console.log(hasil);

// CONTOH LAIN
function jmlhBil(){
    return arguments;
}
var bil = jmlhBil(10, 20, 30, 'hi', false, 17.5);
console.log(bil);

// // LATIHAN 
// var a = 8;
// var b = 3;
// var c = 5;
// var d = 10;
// var volumeA;
// var volumeB;
// var volumeC;
// var volumeD;
// var total;

// volumeA = a*a*a;
// volumeB = b*b*b;
// volumeC = c*c*c;
// volumeD = d*d*d;
// total = volumeA + volumeB + volumeC + volumeD;
// console.log(total);

