/*
!PENGKONDISIAN PADA JAVASCRIPT
- If
    merupakan statment untuk mengeksekusi blok kode jika kondisiniya bernilai true
- if else
    merupakan statement jika suatu kondisi bernilai true dan mengeksekusi blok kode lain jika kondisinya bernilai false
- else If
    merupakan statement jika suatu kondisi sebelumnya false dan kondisi saat ini bernilai true
- Swtich
    untuk mengevaluasi suatu expresi dan mengeksekusi blok kode yang sesuai dengan nilai expresi tersebut
- Ternary Operator
    untuk mengevaluasi suatu kondisi dan mengembalikan nilai yang berbeda-beda tergantung dengan kondisi  tersebut
*/

// CONTOH IF
var usia = 21;
    if (usia >= 20) {
    console.log("Ya, usia anda sekarang adalah " + usia);
    }
console.log("=================");
//CONTOH IF ELSE
var usia2 = 21;
    if (usia2 <= 20) {
    console.log("Ya, usia anda sekarang adalah " + usia2);
    } else {
    console.log("salah, usia anda belum " + usia2);
    }
    console.log("=================");
    // CONTOH ELSE IF
    var tinggi = 150;
    if (tinggi >= 170) {
    console.log("Ya, tinggi badan anda adalah " + tinggi);
    } else if (tinggi <= 170) {
    console.log("Tidak, tinggi badan anda belum mencapai " + tinggi);
    } else {
    console.log(tinggi + " Badan anda belum ideal ");
}

//CONTOH SWITCH
var color = "red";
    switch (color) {
    case "red":
        console.log("Ini adalah warna merah");
        break;
    case "blue":
        console.log("Ini adalah warna biru");
        break;
    case "green":
        console.log("The color is green");
        break;
    default:
        console.log("The color is not red, blue, or green");
}
