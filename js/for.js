var jmlAngkot = 10;
var angkotTidakBeroprasi = 6;
var angkot = 1;
// CONTOH WHILE
while (angkot <= angkotTidakBeroprasi) {
    console.log(
        'Angkot NO. ' + angkot + ' Beroperasi Dengan Baik'
    );
    angkot++;
}
// CONTOH FOR
for (
    angkot = 7;
    angkot <= jmlAngkot;
    angkot++
) {
    console.log(
        'Angkot NO. ' + angkot + ' Tidak Beroprasi'
    );
}