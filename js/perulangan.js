/*
! PERULANGAN PADA JAVASCRIPT
- while
- for
*/
var nomor = 5;
while (nomor < 10) {
    console.log('Mengulang sebanyak ' + nomor + 'x');
}
nomor++;